/*
 * const is a keyword is used to lock the data or the connection between the 
 * pointer and data
 */
#include<stdio.h>
void main(){
	int x=10;
	const int* const ptr=&x;
	printf("%d\n",x);
	x=30;
	printf("%d\n",x);
}
/* No error no warning
	Op: 10
	    30

	x=10(100,103)
      	      ^
	      |
	     lock
	      |
	     ptr =lock=> 100;

	in the above example we can change the direct x
	but cant change the value of x using pointer or the address of ptr

*/
