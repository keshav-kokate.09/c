/*
 * const is a keyword is used to lock the data or the connection between the 
 * pointer and data
 */
#include<stdio.h>
void main(){
	int x=10;
	const int *ptr=&x;
	printf("%d\n",x);
	*ptr=&x;
	printf("%d\n",x);
}
/* 
error: assignment of read-only location '*ptr'
  *ptr=&x;
      ^

	x=10(100,103)
      	      ^
	      |
	     lock
	      |
	     ptr=100;

	in the above example we can change the direct x or the value of ptr


*/
