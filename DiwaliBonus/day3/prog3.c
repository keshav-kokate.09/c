/*WAP which shows the concept of Pointer to an array.*/

#include<stdio.h>
void main(){
/*
 * pointer to an array : a pointer wich stores the address of an array
 */

	int arr[]={10,20,30,40,50};
	//If we want to store an address of whole array we need an array of pointer
	int(*ptr)[]=&arr;

	//name of an array returns an address of first variable so it can be stored in a normal pointer
	int *ptr1=arr;

	printf("%d ",*ptr1);
	printf("%d ",**ptr);

	


}
