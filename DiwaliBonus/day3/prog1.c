/*WAP to create array of size n given by user and take values from
user and print array using pointer.*/

#include<stdio.h>
void main(){
	int r=0;
	printf("Enter size of arry:\n");
	scanf("%d",&r);

	int arr[r];
	printf("Enter array Elements :\n");

	for(int i=0; i<r; i++){
		scanf("%d",arr+i);
	}

	printf("Array elements are : ");
	for(int p=0; p<r; p++){
		printf("%d ",*(arr+p));
	}
}
