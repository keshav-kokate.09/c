/*WAP which explain the concept of array of pointer.*/

#include<stdio.h>
void main(){
/*
 * Array of pointers : an array wich has pointers value in it
 */

	int x=10;
	int y=15;
	int z=20;

	int *arr[]={&x,&y,&z};

	printf("%d\n",**(arr+0));
	printf("%d\n",**(arr+1));
	printf("%d\n",**(arr+2));


}
