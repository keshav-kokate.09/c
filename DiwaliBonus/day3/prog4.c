/*WAP which shows the concept of array of pointer to an array*/

#include<stdio.h>
void main(){
	int arr1[]={10,20,30};
	int arr2[]={40,50,60};
	int arr3[]={70,80,90};

	int (*ptr[])[]={&arr1,&arr2,&arr3};

	printf("%d",**ptr[0]);
}
