/*WAP to create array of size n given by user and take the value from
user sort the array in ascending order and print the sorted array.*/

#include<stdio.h>
void main(){
	int s=0;
	printf("Enter size of an Array :\n");
	scanf("%d",&s);

	int arr[s];
	printf("Enter the arry Elements :\n");
	for(int i=0; i<s; i++){
		scanf("%d",arr+i);
	}
//10 30 20 50 40
	int flag =s;
	while(flag>1){
		for(int i=0; i<s; i++){
			if(*(arr+i)>*(arr+(i+1))){
				int temp=*(arr+i);
				*(arr+i)=*(arr+(i+1));
				*(arr+(i+1))=temp;
				flag++;
			}else{
				flag--;
			}
		}
	}


	printf("Sorted arry:\n");
	for(int j=0; j<s; j++){
		printf("%d ",*(arr+j));
	}

}
