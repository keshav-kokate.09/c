/*WAP to compare two arrays by with length and elements given by
user if array are equal then print “Equal” otherwise print “Not
Equal”.*/

#include<stdio.h>
int arrSize(int (*ptr)[]){
	return sizeof(ptr)/sizeof(int);
}

void main(){
	int s1,s2;
	printf("Enter the size of array1 \n");
	scanf("%d",&s1);
	int arr1[s1];
	for(int i=0; i<s1; i++){
		scanf("%d",&arr1[i]);
	}
	
	printf("Enter the size of array2 \n");
	scanf("%d",&s2);
	int arr2[s2];
	for(int i=0; i<s2; i++){
		scanf("%d",&arr2[i]);
	}

	int flag=0;
	if(arrSize(&arr1)==arrSize(&arr2)){
		for(int i=0; i<s1; i++){
			if(arr1[i]!=arr2[i]){
				flag=1;
				break;
			}
		}
	}else{
		flag=1;
	}


	if(flag){
		printf("Not Equal");
	}else{
		printf("Equal");
	}
}
