/*WAP to create 3-D array of size given by user and print the sum of
all left diagonal elements.*/

#include<stdio.h>
void main(){
	int p,r,c;
	printf("Enter plane, row and col :\n");
	scanf("%d,%d,%d",&p,&r,&c);
	
	int sum=0;
	printf("Enter Array elements :\n");
	int arr[p][r][c];
	for(int i=0; i<p; i++){
		for(int j=0; j<r; j++){
			for(int k=0; k<c; k++){
				scanf("%d",&arr[i][j][k]);
				if(j==k){
					sum=sum+arr[i][j][k];
				}
			}
		}
	}
	
	for(int i=0; i<p*r*c; i++){
		printf("%d ",*(*(*arr)+i));
	}

	printf("\nSum of array diagonal is %d",sum);
}
