/*WAP to create 2-D array of size given from user and assign that
array to another array of same size and print the second array.*/

#include<stdio.h>
void main(){
	int r=0,c=0;

	printf("Enter the row and col :(r,c) \n");
	scanf("%d %d",&r,&c);

	int arr[r][c];
	
	printf("Enter array elements \n");
	for(int i=0; i<r; i++){
		for(int j=0; j<c; j++){
			scanf("%d",&arr[i][j]);
		}
	}
	
	int (*arr1)[]=arr;
	printf("array elements \n");
	for(int j=0; j<r*c; j++){
		printf("%d ",*(*arr1+j));
	}


}
