/*WAP to create 1-D array of length n from user and also take value
from user and print whole 1-D array.*/

#include<stdio.h>
void main(){
	int s=0;
	printf("Enter size of An array\n");
	scanf("%d",&s);

	int arr[s];

	for(int i=0; i<s; i++){
		scanf("%d",&arr[i]);
	}
	
	for(int i=0; i<s; i++){
		printf("%d  ",*(arr+i));
	}
}
