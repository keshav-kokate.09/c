/*WAP to take 1-D array from user and print the second largest
element of the array.*/

#include<stdio.h>
void main(){
	int s;
	printf("Enter array size :\n");
	scanf("%d",&s);

	int arr[s];
	printf("Enter array Elements :\n");
	for(int i=0; i<s; i++){
		scanf("%d",&arr[i]);
	}

	int l1=0,l2=0;

	for(int i=0; i<s; i++){
		if(l1 < *(arr+i)){
			l2=l1;
			l1=*(arr+i);
		}else if(l2<*(arr+i)){
			l2=*(arr+i);
		}
	}

	printf("largest = %d, 2nd Largest =%d",l1,l2);
}
