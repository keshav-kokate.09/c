/*WAP and take one integer pointer, two integer pointer to an pointer and store address in it, Print that Data by
dereferencing all three Pointer.*/

#include<stdio.h>
void main(){
	int x=20;
	int *ptr1=&x;
	int **ptr2=&ptr1;
	int ***ptr3=&ptr2;

	printf("%d\n",*ptr1);
	printf("%d\n",**ptr2);
	printf("%d\n",***ptr3);
}
