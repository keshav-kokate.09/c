/*  WAP which shows a concept of void pointer and access data using void pointer(Don’t repeat class example).*/

#include<stdio.h>
void main(){
	char ch='A';
	void *ptr=&ch;

	char ch1=*(char*)ptr;
	printf("%c\n",ch1);
}
