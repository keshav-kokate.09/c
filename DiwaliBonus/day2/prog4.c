/*WAP of every type of pointer and draw a suitable diagram of that. (Wild pointer, null pointer, dangling pointer,
void pointer)*/

#include<stdio.h>
int *ptr4;

int* fun(){
	int y=20;
	ptr4=&y;
	printf("%d \n",*ptr4);
	return &y;
}

void main(){
	int x=10;
	fun();
	printf("%d \n",*ptr4);
	ptr4=&x;   //Dagling pointer after end of main function it still pointing that address 
	printf("%d \n",*ptr4);
	ptr4=fun(); // dagling pointer after ending fun() the pointer to pointing that address
	printf("%d \n",*ptr4);
	int *ptr1; //wild pointer
	int *ptr2=NULL; //null pointer
	void *ptr3=ptr1; //void pointer


}
