/* Take an array of void pointer and store data of multiple datatype of element(int, char ,float) & Print the array.*/

#include<stdio.h>
void main(){
	int x=10;
	char y='A';
	float f=20.325;

	void *ptr1=&x;
	void *ptr2=&y;
	void *ptr3=&f;

	void *ptr[]={ptr1,ptr2,ptr3};

	printf("%d\n",*(int*)ptr[0]);
	printf("%c\n",*(char*)ptr[1]);
	printf("%lf\n",*(float*)ptr[2]);


}
