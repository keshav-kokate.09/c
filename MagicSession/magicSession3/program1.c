/*WAP to make an array of the table of Given Number
Print the array using a pointer
Input: 2.
Output : 2 4 6 8 1 0 1 2 1 4 1 6 1 8 20
*/

#include<stdio.h>
void main(){
	int t=0;
	printf("Enter the table : \n");
	scanf("%d",&t);

	int arr[10];

	for(int i=0; i<10; i++){
		arr[i]=t*(i+1);
		printf("%d ",*(arr+i));
	}
}
