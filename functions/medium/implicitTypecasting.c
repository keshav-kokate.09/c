#include<stdio.h>
void fun(int x){
	printf("%d\n",x);
}

void main(){
	fun('A');
	fun(65);
	fun(20.5f); //implicit typecast to int
	fun(35.5);  //implicit typecast to int 
}
