//int (* (*)[2])[4]

#include<stdio.h>
void funArr(int (* (* ptr)[])[4]){
	printf("%p",ptr);
}
void main(){
	int arr[4]={10,20,30,40};
	int (* ptr[2])[4]={&arr,&arr};
	funArr(&ptr);
}
