#include<stdio.h>
void fun(int *ptr){
	printf("%p",ptr);
}

void main(){
	int x=10;
	fun(x);
}

/*
 * If the func argument is must match with the parameter declaration
 * if its normal variable, it can implicitely typecast
 */
