/*
 * Return type is be there in the function
 */

#include<stdio.h>
work(){
	printf("Working.....\n");
}
void main(){
	printf("Start main\n");
	work();
	printf("End main\n");
}

/*
 * work dont have the any return type but at the compiled level default 
 * returntype (int) is attched to the function if not declared
 * no need to write return keyword
 */
