/*
 * Return type is be there in the function
 */

#include<stdio.h>
float work(){
	printf("Working.....\n");
}
void main(){
	printf("Start main\n");
	float r=work();
	printf("return value : %lf\n",r);
	printf("End main");
}

/*
 * no need to write return keyword if we dont want to return anything
 * int and char returns by default 0
 * and float return 1.#QNAN0
 */
