/*
 * WAP to check the number is Automorphic or not.
 * note : 6 = 6 * 6 =36 % 10 =6
 *  if (6 == 6){
 *	Automorphic
 *  }
 */

#include<stdio.h>
void main(){
	int num=0;
	printf("Enter Num : \n");
	scanf("%d",&num);

	int temp=num,nRev=0,nCount=0;
	while(temp>0){
		nCount++;
		int d= temp%10;
		nRev = nRev*10+d;
		temp/=10;
	}

	int sqr = num*num;
	int sRev=0;
	while(nCount>0){
		int d=sqr%10;
		sRev = (sRev*10)+d;
		nCount--;
		sqr/=10;
	}

	if(nRev == sRev){
		printf("%d,%d\n",nRev,sRev);
		printf("The Number %d is Automorphic Number\n",num);
	}

}
