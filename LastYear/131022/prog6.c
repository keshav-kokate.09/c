/*
 * WAP to check weather the number is Duck or Not
 * note : 
 * if number contains ZERO(0), excepting leading then the number is Duck
 * eg : 1203, 452013 
 * exception : 0012, 015487
 */

#include<stdio.h>
void main(){
	int num=0;
	printf("Enter the num :\n");
	scanf("%d",&num);
	int flag=0;
	//remove leading zeros
	num=num*1;
	while(num>0){
		int d=num%10;
		if(d == 0){
			flag=1;
			break;
		}
		num/=10;
	}

	if(flag){
		printf("The number is Duck");
	}else{
		printf("The number is Not Duck");
	}

}
