/*
 * Pascals tramgle, or number
 *
 *           1
 *          1 1
 *         1 2 1
 *        1 3 3 1
 *       1 4 6 4 1
 *
 * Note : 121 = 11*11, 1331=121*11
 * pre =1;
 * post = pre*11;
 * pre = post;
 */
