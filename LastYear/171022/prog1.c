/*
 * Amicable numbers
 * 220 and 284
 * Sum of all devisors are equal to each other
 */

#include<stdio.h>
void main(){
	int num1=0,num2=0;
	printf("Enter num1 and num2 \n");
	scanf("%d",&num1);
	scanf("%d",&num2);

	int sum1=0,sum2=0;
	int x=num1/2;

	while(x>0){
		if(num1%x == 0){
			sum1=sum1+x;
		}
		x--;
	}
	
	int y=num2/2;
	while(y>0){
		if(num2%y == 0){
			sum2=sum2+y;
		}
		y--;
	}

	if(sum1 == num2 && sum2 == num1){
		printf("Amicable Numbers");
	}else{
		printf("Sum1 = %d, Sum2= %d",sum1,sum2);
		printf("Not Amicable numbers");
	}

}
