/*
 * WAP to find and replace the numbers in given long number
 * IP : Num = 1234563214
 * 	fNum=2
 * 	rNum=5
 * OP : Num = 1534563514
 *  Note: if the fnum is not in given num op is enter correct fNum  
 */

#include<stdio.h>
void main(){
	long num=0; int fNum=0,rNum=0;
	printf("Enter the Num : \n");
	scanf("%ld",&num);
	printf("Enter find Num And replace num: \n");
	scanf("%d,%d",&fNum,&rNum);

	long x=num,revNum=0;
	int flag=0;
	while(x>0){
		int d=x%10;
		if(d == fNum){
			d=rNum;
			flag++;
		}
		revNum=(revNum*10)+d;
		x/=10;
	}

	if(flag){
		long op=0;
		while(revNum>0){
			op=(op*10)+(revNum%10);
			revNum/=10;
		}
		printf("%ld\n",op);
	}else{
		printf("Enter the correct Find Num input");
	}


}
