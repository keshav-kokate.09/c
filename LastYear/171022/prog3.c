/*
 * WAP to print all prime factors
 * IP : 10;
 * OP : 2,5;
 */

#include<stdio.h>
int isPrime(int num){
	int flag =0;
	for(int i=2; i<num/2; i++){
		if(num%i==0){
			flag++;
		}
	}
	if(flag)
		return 1;
	else
		return 0;
}

void main(){
	int num=0;
	printf("Enter the num:\n");
	scanf("%d",&num);

	int x=num/2;
	while(x>0){
		if(num % x == 0 && isPrime(x)){
			printf("%d ",x);
		}
		x--;
	}

}
