/*
 * 1
 * 3 2 1 
 * 5 4 3 2 1
 * 3 2 1
 * 1
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter no of rows:\n");
	scanf("%d",&row);

	char ch='A';

	for(int i=1; i<row*2; i+=2){
		if(i<=row){
			int x=i;
			for(int j=1; j<=i; j++){
				printf("%d ",x--);
			}
		}else{
			int x=row*2-i;
			for(int j=i; j<row*2; j++){
				printf("%d ",x--);
			}
		}
		printf("\n");
	}
}
