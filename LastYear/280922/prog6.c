/*
 * print the factors of even digit from the given number
 */

#include<stdio.h>
void main(){
	int num=0;
	printf("Enter the num:\n");
	scanf("%d",&num);

	while(num>0){
		int d=num%10;
		if(d%2==0){
			if(d==0){
				printf("Factorial of %d is %d\n",d,1);
			}else{
				int fact=1;
				int f=d;
				while(f>0){
					fact=fact*f;
					f--;
				}

				printf("Factorial of %d is %d\n",d,fact);
			}
		}
		num/=10;
	}
}
