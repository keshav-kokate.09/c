/*
 * print the min digit from the given number
 */

#include<stdio.h>
void main(){
	int num=0;
	printf("Enter the num:\n");
	scanf("%d",&num);
	
	int min=num%10;

	while(num>0){
		int d=num%10;
		if(d<min){
			min=d;
		}
		num/=10;
	}

	printf("Minimum digit is %d",min);
}
