/*
 * 1
 * 3 2 1 
 * 5 4 3 2 1
 * 3 2 1
 * 1
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter no of rows:\n");
	scanf("%d",&row);


	for(int i=1; i<=row; i++){
		if(i<row/2+1){
			int x=i;
			for(int j=1; j<=i; j++){
				printf("%d ",x--);
			}

		}else{
			int x=row-i+1;
			for(int j=i; j<=row; j++){
				printf("%d ",x--);
			}
		}
		printf("\n");
	}
}
