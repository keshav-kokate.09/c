/*
 * 1
 * 1 4
 * 4 7 10
 * 10 13 16 19
 * 19 22 25
 * 25 28
 * 28
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter no of rows:\n");
	scanf("%d",&row);

	int x=1;
	for(int i=1; i<row*2; i++){
		if(i<row){
			for(int j=1; j<=i; j++){
				printf("%d ",x);
				x=x+3;
			}

		}else{
			for(int j=i; j<row*2; j++){
				printf("%d ",x);
				x=x+3;
			}
		}
		x=x-3;
		printf("\n");
	}
}
