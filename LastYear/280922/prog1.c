/*
 * A A A A A A A A A
 *   B B B B B B B
 *     C C C C C
 *       D D D 
 *         E
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter no of rows:\n");
	scanf("%d",&row);

	char ch='A';

	for(int i=1; i<row*2; i+=2){
		for(int j=1; j<row*2; j++){
			if(i>j){
				printf(" ");
			}else{
				printf("%c ",ch);
			}
		}
		printf("\n");
		ch++;
	}
}
