/*
 *         1
 *       3 2 1
 *     5 4 3 2 1
 *   7 6 5 4 3 2 1
 *
 */


#include<stdio.h>
void main(){
	int row=0;
	printf("Enter the rows :\n");
	scanf("%d",&row);

	for(int i=1; i<row*2; i+=2){
		int x=i;
		for(int j=row*2-1; j>=1; j--){
			if(i<j){
				printf(" ");
			}else{
				printf("%d ",x--);
			}

		}
		printf("\n");
	}
}
