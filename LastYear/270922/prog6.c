/*
 * WAP to print the factors of odd digit from given number
 */

#include<stdio.h>
void main(){
	int num=0;
	printf("Enter the num :\n");
	scanf("%d",&num);

	int digit=num,count=0;

	while(digit>0){
		digit/=10;
		count++;
	}

	int arr[count];
	int i=0;
	while(num>0){
		int d=num%10;
		if(d%2!=0){
			arr[i]=d;
			i++;
		}
		num/=10;
	}

	for(int j=0;j<i;j++){
		int fact=1;
		int x=arr[j];
		while(x>0){
			fact=fact*x;
			x--;
		}

		printf("Factorial of %d is %d\n",arr[j],fact);
	
	}
}
