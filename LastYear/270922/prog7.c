/*
 * WAP to print the given number is Armstrong or Not
 */

#include<stdio.h>
void main(){
	int num1=0;
	printf("Enter the num :\n");
	scanf("%d",&num1);

	int digit=num1,count=0;
	while(digit>0){
		digit/=10;
		count++;
	}

	int sum=0;
	int num=num1;
	while(num>0){
		int d=num%10;
		int p=1;
		for(int i=count; i>0; i--){
			p=p*d;
		}
		sum=sum+p;
		num/=10;
	}

	if(sum == num1){
		printf("The %d is ArmStrong",num1);
	}else{
		printf("The %d is not ArmStrong",num1);
		
	}
}
