/*
 *        D
 *      d E
 *    D c B
 *  d E f G
 */


#include<stdio.h>
void main(){
	int row=0;
	printf("Enter the rows :\n");
	scanf("%d",&row);

	for(int i=1; i<=row; i++){
		char ch='A'+row-1;
		char ch1='a'+row-1;
		for(int j=row; j>=1; j--){
			if(i<j){
				printf("  ");
			}else{
				if(j%2!=0){
					printf("%c ",ch);
				}else{
					printf("%c ",ch1);
				}
			if(i%2!=0){
				ch--;
				ch1--;
			}else{
				ch++;
				ch1++;
			}
			}

		}
		printf("\n");
	}
}
