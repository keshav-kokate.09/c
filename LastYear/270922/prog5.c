/*
 * A
 * C B A
 * E D C B A
 * C B A
 * A
 */


#include<stdio.h>
void main(){
	int row=0;
	printf("Enter the rows :\n");
	scanf("%d",&row);

	for(int i=1; i<row*2; i+=2){
		if(i<=row){
			char ch ='A'+i-1;
			for(int j=1; i>=j; j++){
				printf("%c ",ch--);
			}
		}else{
			char ch='A'+i-row;
			for(int j=i; j<row*2; j++){
				printf("%c ",ch--);
			}
		}
		printf("\n");
	}
}
