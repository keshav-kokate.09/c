/*
 *     1
 *    123
 *   12345
 *  1234567
 *
 *  hint:
 *        1
 *       1 2
 *      1 2 3
 *     1 2 3 4 
 *    1 2 3 4 5
 *   1 2 3 4 5 6
 *  1 2 3 4 5 6 7
 * 
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter the no of rows :\n");
	scanf("%d",&row);


	for(int i=1; i<row*2; i++){
		int x=1;
		if(i%2!=0){
			for(int j=(row*2)-1; j>=1; j--){
				if(i<j){
					printf(" ");
				}else{
					if(i%2==0){
						break;
					}else{
						printf("%d ",x++);
					}
				}
			
			}
			printf("\n");
		}
			
	}
}
