/*
 * print the strong numbers in given range
 */

#include<stdio.h>
void main(){
	int s=0,e=0;
	printf("Enter the start num :\n");
	scanf("%d",&s);
	printf("Enter the end num :\n");
	scanf("%d",&e);

	for(int i=s; i<=e; i++){

		int sum=0;
		int num=s;
	
		while(num>0){
			int d=num%10;
			int fact=1;
			for(int i=d; i>=1; i--){
				fact=fact*i;
			}
			sum=sum+fact;
			num/=10;
		}
		if(sum == s){	
			printf("%d ",s);
		}
		s++;
	}
}
