/*
 * print the sum of even digits from given number
 */

#include<stdio.h>
void main(){
	int num=0;
	printf("Enter the num :\n");
	scanf("%d",&num);

	int sum=0;
	while(num>0){
		int d=num%10;
		if(d%2==0){
			sum=sum+d;
		}
		num/=10;
	}

	printf("The sum of even digit is : %d\n",sum);
}
