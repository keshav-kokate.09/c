/*
 * A b C d
 *   d E f
 *     F g
 *       g
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter the no of rows :\n");
	scanf("%d",&row);

	char ch='A';
	char ch1='a';

	for(int i=1; i<=row; i++){
		for(int j=1; j<=row; j++){
			if(i>j){
				printf("  ");
			}else{
				if(j%2==0){
					printf("%c ",ch1);
				}else{
					printf("%c ",ch);
				}
				ch++;
				ch1++;
			}
		}
			printf("\n");
			ch--;
			ch1--;
	}
}
