/*
 * print the sum of even digits from given number
 */

#include<stdio.h>
void main(){
	int n=0;
	printf("Enter the num :\n");
	scanf("%d",&n);

	int sum=0;
	int num=n;
	
	while(num>0){
		int d=num%10;
		int fact=1;
		for(int i=d; i>=1; i--){
			fact=fact*i;
		}
		sum=sum+fact;
		num/=10;
	}
	if(sum == n){	
		printf("Number is Strong \n");
	}else{
		printf("Number is Not Strong \n");
	}
}
