/*
 *                  1
 *              3 2 1
 *    	    5 4 3 2 1
 *	7 6 5 4 3 2 1
    9 8 7 6 5 4 3 2 1


hint:
                     1
		    21
		   321
		  4321
		 54321
		654321
	       7654321
	      87654321
	     987654321
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter the no of rows :\n");
	scanf("%d",&row);


	for(int i=1; i<row*2; i++){
		int x=i;
		if(i%2!=0){
		for(int j=row*2-1; j>=1; j--){
			if(i<j){
				printf("  ");
			}else{
				printf("%d ",x--);	
			}
			
		}
			printf("\n");

		}
			
	}
}
