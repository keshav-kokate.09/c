/*
 * A
 * C B A
 * E D C B A
 * G F E
 * I


hint:
    
   A
   B A
   C B A
   D C B A
   E D B C A
   F E D C
   G F E
   H G
   I
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter the no of rows :\n");
	scanf("%d",&row);

	for(int i=1; i<row*2; i++){
		int ch = 64+i;
		if(i%2!=0){
			if(i<=row){
				for(int j=1; j<=i; j++){
					printf("%c ",ch--);
				}
			}else{
				for(int j=i; j<row*2; j++){
					printf("%c ",ch--);
				}
			}
			printf("\n");
		}
			
	}
}
