/*
 * 3 
 * 2 3
 * 1 2 3
 * 0 1 2 3
 * 1 2 3
 * 2 3
 * 3
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter Rows:\n");
	scanf("%d",&row);

	int y=1;

	for(int i=1; i<=row; i++){
		int x=(row/2)+1-i;
		if(i<=row/2+1){
			for(int j=1; i>=j; j++){
				printf("%d ",x++);
			}
			x--;
		}else{
			for(int j=i; j<=row; j++){
				printf("%d ",y++);
			}
			y--;
		}
		printf("\n");
	}
}
