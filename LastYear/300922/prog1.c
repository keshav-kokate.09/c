/*
 *       10
 *      I H
 *    7 6 5
 *  D C B A
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter Rows:\n");
	scanf("%d",&row);

	int n = row;
	int sum=0;

	while(n>0){
		sum=sum+n;
		n--;
	}

	int x=sum;
	char ch ='A'+sum-1;

	for(int i=1; i<=row; i++){
		for(int j=row; j>=1; j--){
			if(i<j){
				printf("  ");
			}else{
				if(i%2==0){
					printf("%c ",ch--);
					x--;
				}else{
					printf("%d ",x--);
					ch--;
				}
			}
		}
		printf("\n");
	}
}
