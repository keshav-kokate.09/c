/*
 * 4
 * 3 3
 * 2 2 2 
 * 1 1 1 1
 * 2 2 2
 * 3 3
 * 4
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter no of rows:\n");
	scanf("%d",&row);
	int x=row/2+1;

	for(int i=1; i<=row; i++){
		if(i<=row/2+1){
			for(int j=1; j<=i; j++){
				printf("%d ",x);
			}
			x--;
		}else{
			for(int j=i; j<=row; j++){
				printf("%d ",x);
			}
			x++;
		}
		printf("\n");
		if(x==0){
			x+=2;
		}

	}
}
