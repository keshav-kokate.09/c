/*
 * WAP to accept 5 integer value from user and compare two conjecutive numbers 
 * if the second of them is lesser that the previous one then the code will exit
 * of the loop and print the unexpected output
 * I = 1 3 5 4
 * O = loop is exited with input 4
 */

#include<stdio.h>
void main(){
	int s=0;
	printf("Enter size of eleents :\n");
	scanf("%d",&s);

	int n1=0,n2=0;
	scanf("%d",&n1);
	for(int i=1; i<s; i++){
		scanf("%d",&n2);

		if(n1>n2){
			printf("Loop is exited with input %d",n2);
			break;
		}
	}
}
