/*
 * 1
 * 1 2 
 * 2 3 5
 * 5 7 10 15
 * 15 20 27 37 52
 */


#include<stdio.h>
void main(){
	int row=0;
	printf("Enter no of row:\n");
	scanf("%d",&row);

	int x=1;
	for(int i=1; i<=row; i++){
		for(int j=1; j<=row; j++){
			if(j<=i){
				printf("%d ",x);
				x=x+1;
			}
		}
		x--;
		printf("\n");
	}
}
