/*
 *            1
 *          2 1 2
 *        3 2 1 2 3
 *      4 3 2 1 2 3 4
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter no of rows:\n");
	scanf("%d",&row);

	for(int i=1; i<=row*2; i+=2){
		int x=row;
		for(int j=row*2; j>=1;j--){
			if(j>i){
				printf(" ");
			}else{
				printf("%d ",x--);
				if(j<=row){
					x+=2;
				}
			}
		}
		printf("\n");
	}
}
