/*
 * WAP to replace every occerance of 1 by 2
 * IP : 45687512351
 * oP : 45687522352
 */

#include<stdio.h>
void main(){
	long num=0;
	printf("Enter num :\n");
	scanf("%ld",&num);

	long temp=num,rev=0;
	while(temp>0){
		int d=temp%10;
		if(d==1){
			d=2;
		}
		rev=(rev*10)+d;
		temp/=10;
	}

	long op=0;
	while(rev>0){
		int d=rev%10;
		op=op*10+d;
		rev/=10;
	}

	printf("%ld",op);
}
