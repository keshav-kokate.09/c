/*
 * WAP to swap first and last digit of given number.
 * IP: 9745123548
 * OP: 8745123549
 */

#include<stdio.h>
void main(){
	long num=0;
	printf("Enter num :\n");
	scanf("%ld",&num);

	long x=num,mult=1,first=0,last=0;

	last=num%10;
	while(x>0){
		mult=mult*10;
		x/=10;
	}

	mult/=100;
	long op=last;
	num/=10;
	op=(op*mult)+(num % mult);
	num/=mult;
	op=(op*10)+num%10;

	printf("%ld",op);

}
