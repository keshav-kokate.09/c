/*   	   #
 *    	 * # *
 *     * * # * * 
 *   * * * # * * *
 *   r=4;
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter no of rows :\n");
	scanf("%d",&row);

	for(int i=1; i<row*2; i+=2){
		for(int j=row*2-1; j>=1; j--){
			if(i<=j){
				printf(" ");
			}else{
				if(j == row){
					printf("# ");
				}else{
					printf("* ");

				}
			}
		}
		printf("\n");
	}

}
