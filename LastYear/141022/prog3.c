/*
 *         D
 *       C D C
 *     B C D C B
 *   A B C D C B A
 *     B C D C B 
 *       C D C
 *         A
 *
 *   r=7;
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter No Of Rows:\n");
	scanf("%d",&row);

	for(int i=1; i<row*2; i+=2){
		char ch ='A';
		if(i<=row){
			for(int j=row*2-1; j>=1; j--){
				if(i<j){
					printf(" ");
				}else{
					printf("%c ",ch);
				}
			
				if(j>=row){
				ch++;
			}else{
				ch--;
			}
			}
		}else{
			for(int j=i; j<row*2; j++){
				if(i>j){
					printf(" ");
				}else{
					printf("%c ",ch);
				}

			if(j>=row){
				ch++;
			}else{
				ch--;
			}
			}

		}
		printf("\n");
	}
}
