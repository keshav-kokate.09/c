/*
 * WAP to print weather the given number is DUCK or NOT
 * Note : Duck number wich contains Zero in the number
 * 	eg : 12054 : DUCK
 * 	     0012  : NOT DUCK
 */


#include<stdio.h>
void main(){
	long num=0;
	printf("Enter the number :\n");
	scanf("%ld",&num);

	//Remove leading zeroes 
	num = num*1;

	int flag=0;
	while(num>0){
		int d =num%10;
		if(d==0){
			flag=1;
			break;
		}
		num/=10;
	}


	if(flag)
		puts("Number is DUCK");
	else
		puts("Number is DUCK");

}
