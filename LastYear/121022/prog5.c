/*
 *     *
 *   * *
 * * * *
 *   * *
 *     *
 *   * * 
 * * * *
 *   * *
 *     *
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("EnterNo of rows :\n");
	scanf("%d",&row);
	
	int x=row/3;

	for(int i=1; i<=row; i++){
		for(int j=1; j<=row/3; j++){
    			if(j<x){
				printf("  ");
			}else{
				printf("* ");
			}
		
		}
		if(i<row/3 || (i<=row/3*2 && i>row/2)){
			x--;
		}else{
			x++;
		}
		printf("\n");
	}

}
