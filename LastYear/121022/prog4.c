/*
 *       3
 *     2 2
 *   1 1 1
 *     2 2
 *       3 
 *     2 2
 *   1 1 1
 *     2 2
 *       3
 */

#include<stdio.h>
void main(){
	int row=0;
	printf("Enter no of rows:\n");
	scanf("%d",&row);

	int x=row/3;
	for(int i=1; i<=row; i++){
		for(int j=1; j<=row/3;j++){
			if(j<x){
				printf("  ");
			}else{
				printf("%d ",x);
			}
		}
		printf("\n");
		if(i<row/3 || (i<=row/3*2 && i>row/2)){
			x--;
		}else{
			x++;
		}

	}
}
