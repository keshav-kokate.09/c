/*
 * Threre are mainly two types of string initialization
 * Using Char * and cha[]
 * Char * stores the string into ROdata section which is similer
 * to SCP(String contant pool)
 */

#include<stdio.h>
void main(){
	char str1[]={"Keshav"};
	char str2[]={'K','E','s','h','a','v','\0'};
	char *str3="Keshav";

	puts(str1);
	printf("%p\n",str1);
	puts(str2);
	printf("%p\n",str2);
	puts(str3);
	printf("%p\n",str3);
}
