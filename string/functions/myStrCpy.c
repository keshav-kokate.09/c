/*
 * Copy the string from src to dest;
 */

#include<stdio.h>
char *myStrCpy(char *dest, char *src){
	while(*src !='\0'){
		*dest = *src;
		dest++;
		src++;
	}
	*dest='\0';
	return dest;
}

void main(){
	char str1[20];
	char * str2="Keshav";

	myStrCpy(str1,str2);

	puts(str1);
	puts(str2);
}
