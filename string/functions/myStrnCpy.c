/*
 * Copy upto n characters to the dest string
 */

#include<stdio.h>

char * myStrNCpy(char *dest,char* src,int n){
	while(n>0 && *src!=0){
		*dest = *src;
		dest++;
		src++;
		n--;
	}
	*dest='\0';
	return dest;
}

void main(){
	char str1[20];
	char *str2="Keshav Kokate";
	
	myStrNCpy(str1,str2,6);

	puts(str1);
	puts(str2);
}
