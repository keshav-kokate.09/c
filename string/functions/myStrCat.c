/*
 * Concanating one string into another string
 * steps : iterate dest string until \0
 *         copy one by one until src != \0
 */

#include<stdio.h>
char *myStrCat(char *dest, char *src){
	while(*dest !='\0')
		dest++;

	while(*src !='\0'){
		*dest =*src;
		dest++;
		src++;
	}
	*dest='\0';
	return dest;
}

void main(){
	char str1[20]="Keshav";
	char * str2="Kokate";

	puts(str1);
	puts(str2);
	myStrCat(str1,str2);
	puts(str1);
	puts(str2);
}
