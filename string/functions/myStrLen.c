/*
 * Calculate the length of String
 */

#include<stdio.h>
int myStrLen(char *str){
	int count=0;
	while(*str!='\0'){
		count++;
		str++;
	}
	return count;
}

void main(){
	char * str="Keshav";
	int len =myStrLen(str);
	printf("%d\n",len);
}
