/*
 * Static Storage class :
 * 	1. static storage class can further divided into local static storage class
 * 	and global static storage class
 * 	2. static local storage class : scope is limited to that function only, it initialises only once, and stores the value into the ReadOnly data. (refer prog2.c)
 * 	3. static global : scope is limited to that file only, it cannot accsesible by another file using extern keyword also (rafer prog3.c, prog4.c)
 *
 */

#include<stdio.h>
void fun(){
	static int x=10;
	x++;
	printf("%d ",x);
}

void main(){
	fun();
	fun();
	fun();
}
