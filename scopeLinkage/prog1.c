/* auto storage class :
 * 	1. It is by defult storage class for local variables only.
 * 	2. It stores the value into the stack of the var declared function
 * 	3. By default value is allways a garbage value of local variables
 */

#include<stdio.h>
//auto int z=20;
//error: file-scope declaration of 'z' specifies 'auto';

auto void fun(){
	printf("Test\n");
}

void main(){
	auto int x=10;
	printf("%d",x);
	fun();
	//auto fun();
}
 
